import React from "react";
import { UncontrolledCarousel } from "reactstrap";

//import img from Images/images.jpg

const items = [
  {
    src: "https://source.unsplash.com/user/erondu/1600x500",
    altText: "Slide 1",
    caption: "NO PAIN NO GAIN",
    header: "SK-GYM",
    key: "1",
  },

  {
    src: "https://source.unsplash.com/user/erondu/1600x500",
    altText: "Slide 2",
    caption: "NO PAIN NO GAIN",
    header: "SK-GYM",
    key: "2",
  },
  {
    src: "https://source.unsplash.com/user/erondu/1600x500",
    altText: "Slide 3",
    caption: "NO PAIN NO GAIN",
    header: "SK-GYM",
    key: "3",
  },
];

const Slider = () => <UncontrolledCarousel items={items} />;

export default Slider;
