import React, { Component } from 'react'
import { Container } from 'reactstrap';
import LoginService from "../Services/Login/LoginService";
import Hooks from 'react';


import Footer from './Footer';
import adminscreen from '../screesn/adminScreen';
class LoginComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            umtEmail :'',
            umtPassword :''
        }
        this.onLogin=this.onLogin.bind(this);
    }
    
    onLogin =(e) => {
        e.preventDefault();
        let log={umtEmail: this.state.umtEmail,umtPassword:this.state.umtPassword};
        LoginService.Login(log).then(res=>{
            let umt_role = res.data.result;
            //console.log('login')
            umt_role === "ADMIN" && this.props.history.push("/admin");
            umt_role=== "MEMBER" && this.props.history.push('/member');
            console.log('login')

            umt_role === "TRAINER" && this.props.history.push('/trainer');
        });
    }


    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render(){
        return (
            <div>
                <Container>
                <form>
                <div className="form-group">
                    <label>Email:</label>
                    <input type="email" placeholder="userName" name="umtEmail" className="form-control" value={this.state.umtEmail} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password:</label>
                    <input type="password" placeholder="password" name="umtPassword" className="form-control" value={this.state.umtPassword} onChange={this.onChange}/>
                </div>
                <button className="btn btn-success" onClick={this.onLogin}>Login</button>
                
            </form>
            </Container>
            <Footer />
            </div>
        );
    }
 }

export default LoginComponent;