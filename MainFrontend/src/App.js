//import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import HomeScreen from './screesn/HomeScreen'
//import RegistrationScreen from './screesn/RegistrationScreen'
import AboutScreen from './screesn/AboutScreen'
import Navigation from './components/Navigation'
import memberScreen from './screesn/memberScreen'
import adminScreen from './screesn/adminScreen'


//import "../node_modules/bootstrap/dist/css/bootstrap/min.css"
import LoginComponent from './components/LoginComponents';
import trainerScreen from './screesn/trainerScreen';

import Signup from './screesn/Signup';

function App() {

  return (
   
    
    <Router>
    <div> 
      <Navigation/>
      <Switch>
        <Route path="/home" component={HomeScreen}/>
        <Route path="/about" component={AboutScreen}/>
        <Route path="/signup" component={Signup}/>
        <Route path="/login" component={LoginComponent}/>
        <Route path ="/member" component={memberScreen}/>
        <Route path ="/admin" component={adminScreen}/>
        <Route path ="/trainer" component={trainerScreen}/>

        

        

      </Switch>
      
    </div>
   
  </Router>
  

  );
  
  
}

export default App;
