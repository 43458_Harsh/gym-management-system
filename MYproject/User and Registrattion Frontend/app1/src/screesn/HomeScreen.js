import Header from '../components/Header';
import Slider from '../components/Slider';
import Footer from '../components/Footer';
import CardComponent from "../components/CardComponent";




const HomeScreen=(props)=>{
    return(
        <div>
            <Header title = "Home"/>
            <Slider />
           <CardComponent 
            
            title="SK-GYM"
             subtitle="GOLD-Package"
             text="Here is a gold package for GYM lover in which you will get 3 month of
             membership period,
             where you will get 2 time massage and SONA and at minimum price of 3000 "
             />
            <Footer title ="footer" />
            
            
        </div>
        
    );
};
export default HomeScreen;