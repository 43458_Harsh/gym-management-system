import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { signup } from '../actions/userAction'
import { useDispatch, useSelector } from 'react-redux'
import { Container } from 'reactstrap';

import Footer from '../components/Footer';





const SignupScreen = (props) => {
  const [membername, setName] = useState('')
  const [memberaddress, setAddress] = useState('')
  const [memberemail, setEmail] = useState('')
  const [memberpassword, setPassword] = useState('')
  const [membercontact, setContact] = useState('')
  const [memberage, setAge] = useState('')
  const [gender, setGender] = useState('')
  const [joining, setJoining] = useState('')
  const [endOfMembershipDate, setEnding] = useState('')
  const [id, setID] = useState('')
  
  const dispatch = useDispatch()

  const userSignup = useSelector((store) => store.userSignup)
  const { loading, response, error } = userSignup

  
  useEffect(() => {
    console.log('use effect called: ')
    console.log('loading: ', loading)
    console.log('response: ', response)
    console.log('error: ', error)

    if (response && response.status == 'success') {
      
      props.history.push('/signin')
    } else if (error) {
     
      console.log(error)
      alert('error while making API call')
    }
  }, [loading, response, error])

  const onSignup = () => {
   
    dispatch(signup(membername,memberaddress,memberemail,memberpassword,membercontact,memberage,gender,joining,endOfMembershipDate,id))
  }

  return (
    <div>
    < Container>
      
        
      <Header title=" User-Signup" />
      <div className="form-group">
      
          <label className="form-label"> memberName</label>
          <input
            onChange={(e) => {
              setName(e.target.value)
            }}
            className="form-control"></input>
        </div>
        
        <div className="mb-3">
        
          <label className="form-label"> memberAddress</label>
          <input
            onChange={(e) => {
              setAddress(e.target.value)
            }}
            className="form-control"></input>
          
        </div>
        <div className="mb-3">
          <label className="form-label">memberEmail</label>
          <input
            onChange={(e) => {
              setEmail(e.target.value)
            }}
            type="email"
            className="form-control"
            placeholder="test@test.com"
          />
        </div>
        <div className="mb-3">
          <label className="form-label">memberPassword</label>
          <input
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            type="password"
            className="form-control"
            placeholder="*****"></input>
        </div>

        <div className="mb-3">
          <label className="form-label">memberContact</label>
          <input
            onChange={(e) => {
              setContact(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> memberAge</label>
          <input
            onChange={(e) => {
              setAge(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> Gender</label>
          <input
            onChange={(e) => {
              setGender(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label> joiningDate</label>
          <input
            onChange={(e) => {
              setJoining(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> endOfMembershipDate</label>
          <input
            onChange={(e) => {
              setEnding(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> ID</label>
          <input
            onChange={(e) => {
              setID(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <button onClick={onSignup} className="btn btn-success">
            Singup
          </button>
          <div className="float-end">
            Existing user? <Link to="/login">login here</Link>
          </div>
        </div>
       
      {userSignup.loading && <div>waiting for result</div>}
     
     
      
    </Container>
    <Footer />
    </div>
    
    
  )
}


export default SignupScreen
