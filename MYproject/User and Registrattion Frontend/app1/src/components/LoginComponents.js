import React, { Component } from 'react'
import { Container } from 'reactstrap';
import LoginService from "../Services/Login/LoginService";

import Footer from './Footer';
class LoginComponent extends Component{
    constructor(props){
        super(props)
        this.state={
            umtEmail :'',
            umtPassword :''
        }
        this.onLogin=this.onLogin.bind(this);
    }
    
    onLogin =(e) => {
        e.preventDefault();
        let log={umtEmail: this.state.umtEmail,umtPassword:this.state.umtPassword};
        LoginService.Login(log).then(res=>{
            this.props.history.push('/Admin');
        });
    }


    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render(){
        return (
            <div>
                <Container>
                <form>
                <div className="form-group">
                    <label>Email:</label>
                    <input type="email" placeholder="userName" name="umtEmail" className="form-control" value={this.state.umtEmail} onChange={this.onChange}/>
                </div>

                <div className="form-group">
                    <label>Password:</label>
                    <input type="password" placeholder="password" name="umtPassword" className="form-control" value={this.state.umtPassword} onChange={this.onChange}/>
                </div>
                <button className="btn btn-success" onClick={this.onLogin}>Login</button>
                
            </form>
            </Container>
            <Footer />
            </div>
        );
    }
 }

export default LoginComponent;