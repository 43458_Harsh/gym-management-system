import {
   
    USER_SIGNUP_FAIL,
    USER_SIGNUP_REQUEST,
    USER_SIGNUP_SUCCESS,
  } from '../constants/userConstants'
  import axios from 'axios'
  
  
  
  export const signup = (name,address,email,password,contact,age,gender,joining,ending,id) => {
    return (dispatch) => {
      dispatch({
        type: USER_SIGNUP_REQUEST,
      })
  
      const header = {
        headers: {
          'Content-Type': 'application/json',
        },
      }
  
      const body = {
        name,
        address, 
        email, 
        password,
        contact,
        age,
        gender,
        joining,
        ending,
        id
      }
      const url = 'http://localhost:7070/user/signup'
      axios
        .post(url, body, header)
        .then((response) => {
          dispatch({
            type: USER_SIGNUP_SUCCESS,
            payload: response.data,
          })
        })
        .catch((error) => {
          dispatch({
            type: USER_SIGNUP_FAIL,
            payload: error,
          })
        })
    }
  }