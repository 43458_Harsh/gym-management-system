import React, { Component } from 'react'


class HomeComponent extends Component{
    constructor(props){
        super(props)
        this.state={

        }
       this.login=this.login.bind(this);
       this.signup=this.signup.bind(this);
    }
    login(){
        this.props.history.push('/login');
    }
    signup(){
        this.props.history.push('/signup');
    }
    render(){
        return(
            <div>
                 <h2 className="text-center">The Workout Zone</h2>
                 <button className="success" style={{width:'100px'}} onClick={() => this.login()}>Login</button>
                 <br/><br/>
                 <button className="success" style={{width:'100px'}} onClick={() => this.signup()}>Signup</button>
            </div>

        )
    }
}
export default HomeComponent;