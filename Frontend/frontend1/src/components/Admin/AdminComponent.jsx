import React, { Component } from 'react'
import axios from 'axios';
import swal from 'sweetalert';
import { Link } from 'react-router-dom'
class AdminComponent extends Component{
    constructor(props){
        super(props)
        this.state={
        
        }
        this.addTrainer=this.addTrainer.bind(this);
        this.addMembership=this.addMembership.bind(this);
        this.listOfMemberships=this.listOfMemberships.bind(this);
        this.listOfTrainers=this.listOfTrainers.bind(this);
    }

    addTrainer =(e) => {
        e.preventDefault();
         this.props.history.push('/addTrainer');
    }
    addMembership =(e) => {
    e.preventDefault();
     this.props.history.push('/addMembership');
    }
    listOfMemberships =(e) => {
        e.preventDefault();
         this.props.history.push('/listofmemberships');
    }
    listOfTrainers =(e) => {
        e.preventDefault();
         this.props.history.push('/listoftrainers');
    }
    logout=(e)=>{
        this.props.history.push('/');
    }
    render(){
        return(
            
            <div><br/>
               <h2 className="text-center">ADMIN</h2>
               <div style={{ display: 'flex', justifyContent: 'flex-end' }}><button className="btn btn-success" onClick={this.logout}>Logout</button></div>
               <button className="btn btn-success" onClick={this.addTrainer}>Add Trainer</button><br/><br/>
               <button className="btn btn-success" onClick={this.listOfTrainers}>List Of Trainers</button><br/><br/>
               <button className="btn btn-success" onClick={this.addMembership}>Add Membership</button><br/><br/>
               <button className="btn btn-success" onClick={this.listOfMemberships}>List Of Memberships</button>
            </div>
        )
    }

}
export default AdminComponent;