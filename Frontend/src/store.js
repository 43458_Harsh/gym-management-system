import { createStore, combineReducers, applyMiddleware } from 'redux'
import { userSignupReducer } from './reducers/userReducers'

//import { addinstructorsReducer, fetchinstructorsReducer } from './reducers/instructorsReducers'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

// combined reducers
const reducers = combineReducers({
  userSignup: userSignupReducer,
  
})

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(logger, thunk))
)

export default store
