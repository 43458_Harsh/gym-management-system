import Footer from '../components/Footer';
import Header from '../components/Header';
import LoginComponent from '../components/LoginComponents';



function LoginScreen(props) {
    return (
        <div>
            <Header title="Login" />
            <LoginComponent />
         <Footer />  
        </div>
         
    );
}
export default LoginScreen