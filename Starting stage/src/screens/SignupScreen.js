import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { signup } from '../actions/userActions'
import { useDispatch, useSelector } from 'react-redux'





const SignupScreen = (props) => {
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [contact, setContact] = useState('')
  const [age, setAge] = useState('')
  const [gender, setGender] = useState('')
  const [joining, setJoining] = useState('')
  const [ending, setEnding] = useState('')
  const [id, setID] = useState('')
  
  const dispatch = useDispatch()

  const userSignup = useSelector((store) => store.userSignup)
  const { loading, response, error } = userSignup

  
  useEffect(() => {
    console.log('use effect called: ')
    console.log('loading: ', loading)
    console.log('response: ', response)
    console.log('error: ', error)

    if (response && response.status == 'success') {
      
      props.history.push('/signin')
    } else if (error) {
     
      console.log(error)
      alert('error while making API call')
    }
  }, [loading, response, error])

  const onSignup = () => {
   
    dispatch(signup(name,address,email,password,contact,age,gender,joining,ending,id))
  }

  return (
    <div>
      <Header title="Signup" />
      <div className="form">
        <div className="mb-3">
          <label className="form-label"> Name</label>
          <input
            onChange={(e) => {
              setName(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label"> Address</label>
          <input
            onChange={(e) => {
              setAddress(e.target.value)
            }}
            className="form-control"></input>
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            onChange={(e) => {
              setEmail(e.target.value)
            }}
            type="email"
            className="form-control"
            placeholder="test@test.com"
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            onChange={(e) => {
              setPassword(e.target.value)
            }}
            type="password"
            className="form-control"
            placeholder="*****"></input>
        </div>

        <div className="mb-3">
          <label className="form-label">Contact</label>
          <input
            onChange={(e) => {
              setContact(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> Age</label>
          <input
            onChange={(e) => {
              setAge(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> Gender</label>
          <input
            onChange={(e) => {
              setGender(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> Joining</label>
          <input
            onChange={(e) => {
              setJoining(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> Ending</label>
          <input
            onChange={(e) => {
              setEnding(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <label className="form-label"> ID</label>
          <input
            onChange={(e) => {
              setID(e.target.value)
            }}
            className="form-control"></input>
        </div>

        <div className="mb-3">
          <button onClick={onSignup} className="btn btn-success">
            Singup
          </button>
          <div className="float-end">
            Existing user? <Link to="/signin">Signin here</Link>
          </div>
        </div>
      </div>

      {userSignup.loading && <div>waiting for result</div>}
    </div>
  )
}

export default SignupScreen
