package com.app.dao;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.pojos.WorkoutPlan;

@Repository
public interface IWorkoutPlanRepository extends JpaRepository<WorkoutPlan,Integer>{
	
	//@Query(value="select * from workoutplans  where member_id=id and workout_date=date",nativeQuery = true)
	@Query("select c from WorkoutPlan c where c.memberId=:id and c.workoutDate=:date")
	Optional<WorkoutPlan> getMyWorkoutPlan(@Param("id")int id, @Param("date")LocalDate today);
	
	@Modifying
	@Query("delete from WorkoutPlan w where w.memberId=:id")
	void deleteWorkoutplanById(@Param("id")int id);
}
