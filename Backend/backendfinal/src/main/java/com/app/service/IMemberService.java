package com.app.service;

import java.util.Optional;

import com.app.pojos.Member;

public interface IMemberService {
	Member signUp(Member member);
    Optional<Member> getProfile(Integer id);
}
