package com.app.service;

import java.sql.Date;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_excs.WorkoutPlanHandlingException;
import com.app.dao.IWorkoutPlanRepository;
import com.app.pojos.WorkoutPlan;
@Service
@Transactional
public class WorkoutplanServiceImpl implements IWorkoutPlanService{

	@Autowired
	IWorkoutPlanRepository workoutplanRepo;
	LocalDate today = LocalDate.now();
	Date date = Date.valueOf(LocalDate.now());
	@Override
	public WorkoutPlan getMyWorkoutplan(int id) {
		System.out.println("date"+today);
		//Optional<WorkoutPlan> myWorkoutPlan = workoutplanRepo.getMyWorkoutPlan(id, today);
		//if(myWorkoutPlan.isPresent())
		//return myWorkoutPlan;
		//return null;
		return workoutplanRepo.getMyWorkoutPlan(id, today).orElseThrow(() -> new WorkoutPlanHandlingException("Invalid Credentials!!!! "));
	}
	@Override
	public WorkoutPlan addWorkoutplan(WorkoutPlan workoutPlan) {
		
		return workoutplanRepo.save(workoutPlan);
	}

}
