package com.app.service;

import java.util.List;

import com.app.dto.TrainerDTO;
import com.app.dto.TrainerUpdateDTO;
import com.app.pojos.Trainer;

public interface ITrainerService {
	List<TrainerDTO> getAllTrainers();
	List<Trainer> getAllTrainerss();
	String addTrainer(Trainer trainer);
	void deleteTrainerDetaild(int TrainerId);
	Trainer updateTrainerDetails(int trainerId,TrainerUpdateDTO trainer);
	
}
