package com.app.service;

import java.util.List;

import com.app.pojos.Membership;

public interface IMembershipService {
	List<Membership> getAllMemberships();
	Membership addMemberships(Membership memberships);
	void deleteMembershipDetail(int membershipId);
}
