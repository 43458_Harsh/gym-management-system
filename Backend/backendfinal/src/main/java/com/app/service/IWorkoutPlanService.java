package com.app.service;

import com.app.pojos.WorkoutPlan;

public interface IWorkoutPlanService {
	
	WorkoutPlan getMyWorkoutplan(int id);
	
	WorkoutPlan addWorkoutplan(WorkoutPlan workoutPlan);
}
