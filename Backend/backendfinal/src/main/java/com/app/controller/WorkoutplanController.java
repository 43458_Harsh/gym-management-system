package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ResponseDTO;
import com.app.pojos.WorkoutPlan;
import com.app.service.IWorkoutPlanService;

@CrossOrigin
@RestController
@RequestMapping("/gym")
public class WorkoutplanController {
	
	@Autowired
	IWorkoutPlanService workoutPlanSer;

	public WorkoutplanController() {
		// TODO Auto-generated constructor stub
	}
	@GetMapping("getmywoplan/{mywoplanid}")
	public ResponseEntity<?> getMyWorkoutPlan(@PathVariable int mywoplanid){
		System.out.println("in getmemberbyid "+mywoplanid);
		//Optional<WorkoutPlan> w=Optional.ofNullable(workoutPlanSer.getMyWorkoutplan(request.getId()).get());
		return new ResponseEntity<>(workoutPlanSer.getMyWorkoutplan(mywoplanid), HttpStatus.OK);
		//if(w.isPresent())
		//return new ResponseEntity<>(w, HttpStatus.OK);
		//return null;
	}
	@PostMapping("/addwoplan")
	public ResponseDTO<?> addWorkoutplan(@RequestBody WorkoutPlan request){
		System.out.println("in getmemberbyid "+request);
		return new ResponseDTO<>(HttpStatus.OK,"Workout plan added",workoutPlanSer.addWorkoutplan(request));
	}
	@DeleteMapping("/deletewo/{memberId}")
	public ResponseDTO<?> deleteWorkoutDetails(@PathVariable int memberId) {
		System.out.println("in del Workoutplan dtls " + memberId);
		try {
			workoutPlanSer.deleteWorkoutDetails(memberId);;
			return new ResponseDTO<>(HttpStatus.OK, "Workoutplans details deleted", null);
		} catch (RuntimeException e) {
			System.out.println("err in delete " + e);
			return new ResponseDTO<>(HttpStatus.INTERNAL_SERVER_ERROR, "Workoutplans details deletion failed", null);
		}
	}
}
