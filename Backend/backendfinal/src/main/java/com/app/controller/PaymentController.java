package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.ResponseDTO;
import com.app.pojos.Payment;
import com.app.pojos.WorkoutPlan;
import com.app.service.IPaymentService;

@CrossOrigin
@RestController
@RequestMapping("/payment")
public class PaymentController {
	@Autowired
	IPaymentService paymentSer;
	
	public PaymentController() {
		
	}
	@PostMapping("/addpayment")
	public ResponseDTO<?> addPayment(@RequestBody Payment request){
		System.out.println("in getmemberbyid "+request);
		return new ResponseDTO<>(HttpStatus.OK,"Payment added",paymentSer.addPayment(request));
	}
}
