package com.app.custom_excs;

public class WorkoutPlanHandlingException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	public WorkoutPlanHandlingException(String errMesg) {
		super(errMesg);
	}
}
