package com.app.pojos;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
@Entity
@Table(name="members")
public class Member {
	@Id //mandatory
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank(message = "Name is required")
	@Length(min = 3, max = 15, message = "Invalid len of Name")
	@Column(length = 20)
	private String memberName;
	
	private String memberAddress;
	@NotBlank(message = "Contact is required")
	@Length(min = 10, max = 10, message = "Invalid length of Contact")
	@Column(length = 10)
	private String memberContact;
	@NotBlank(message = "Email is required")
	@Column(length = 20)
	private String memberEmail;
	
	
	private String memberPassword;
	
	private int memberAge;
	@NotBlank(message = "Gender is required")
	@Length(min = 1, max = 1, message = "Invalid length of Gender")
	@Column(length = 1)
	private String gender;
	@JsonDeserialize(as = LocalDate.class)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate joiningDate;
	@JsonDeserialize(as = LocalDate.class)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endOfMembershipDate;
	@ManyToOne
	@JoinColumn(name = "trainer_id")
	
	private Trainer selectedTrainer;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "member_id")
	private List<Payment> payments = new ArrayList<>();
	@NotNull(message = "Please enter id")
	private int membershipId;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="login_id")
	@JsonIgnore
	private Login login;
	
	
	public Member() {
		System.out.println("in ctor"+getClass().getName());
	}
	public Member(Integer id, String memberName, String memberAddress, String memberContact, String memberEmail,
			String memberPassword, int memberAge, String gender, LocalDate joiningDate, LocalDate endOfMembershipDate) {
		this.id = id;
		this.memberName = memberName;
		this.memberAddress = memberAddress;
		this.memberContact = memberContact;
		this.memberEmail = memberEmail;
		this.memberPassword = memberPassword;
		this.memberAge = memberAge;
		this.gender = gender;
		this.joiningDate = joiningDate;
		this.endOfMembershipDate = endOfMembershipDate;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberAddress() {
		return memberAddress;
	}
	public void setMemberAddress(String memberAddress) {
		this.memberAddress = memberAddress;
	}
	public String getMemberContact() {
		return memberContact;
	}
	public void setMemberContact(String memberContact) {
		this.memberContact = memberContact;
	}
	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public String getMemberPassword() {
		return memberPassword;
	}
	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}
	public int getMemberAge() {
		return memberAge;
	}
	public void setMemberAge(int memberAge) {
		this.memberAge = memberAge;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public LocalDate getJoiningDate() {
		return joiningDate;
	}
	public void setJoiningDate(LocalDate joiningDate) {
		this.joiningDate = joiningDate;
	}
	public LocalDate getEndOfMembershipDate() {
		return endOfMembershipDate;
	}
	public void setEndOfMembershipDate(LocalDate endOfMembershipDate) {
		this.endOfMembershipDate = endOfMembershipDate;
	}
	public Trainer getSelectedTrainer() {
		return selectedTrainer;
	}
	public void setSelectedTrainer(Trainer selectedTrainer) {
		this.selectedTrainer = selectedTrainer;
	}
	public List<Payment> getPayments() {
		return payments;
	}
	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}
	public int getMembershipId() {
		return membershipId;
	}
	public void setMembershipId(int membershipId) {
		this.membershipId = membershipId;
	}
	
	
	public Login getLogin() {
		return login;
	}
	public void setLogin(Login login) {
		this.login = login;
	}
	@Override
	public String toString() {
		return "member Id"+id+" [memberName=" + memberName + ", memberAddress=" + memberAddress + ", memberContact="
				+ memberContact + ", memberEmail=" + memberEmail + ", memberPassword=" + memberPassword + ", memberAge="
				+ memberAge + ", gender=" + gender + ", joiningDate=" + joiningDate + ", endOfMembershipDate="
				+ endOfMembershipDate + "]";
	}
	
}
