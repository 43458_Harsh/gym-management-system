package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;


@Entity
@Table(name="users")
public class User {
	@Id //mandatory
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank(message = "Name is required")
	@Length(min = 3, max = 15, message = "Invalid len of Name")
	@Column(length = 20)
	private String userName;
	@NotBlank(message = "Contact is required")
	@Length(min = 10, max = 10, message = "Invalid length of Contact")
	@Column(length = 10)
	private double userContact;
	@NotBlank(message = "Address is required")
	@Length(min = 3, max = 15, message = "Invalid len of Address")
	@Column(length = 20)
	private String userAddress;
	@NotBlank(message = "Email is required")
	@Column(length = 20)
	private String userEmail;
	@Pattern(regexp = "((?=.*\\d)(?=.*[a-z])(?=.*[#@$*]).{5,20})", message = "Blank or Invalid password")
	@Column(length = 20,nullable = false)
	@JsonIgnore
	private String userPassword;
	
	
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "user_id")
	private List<Membership> memberships = new ArrayList<>();
	
	public User() {
		System.out.println("in ctor"+getClass().getName());
	}

	public User(Integer id, String userName, double userContact, String userAddress, String userEmail, String userPassword) {
		this.id = id;
		this.userName = userName;
		this.userContact = userContact;
		this.userAddress = userAddress;
		this.userEmail = userEmail;
		this.userPassword = userPassword;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public double getUserContact() {
		return userContact;
	}

	public void setUserContact(double userContact) {
		this.userContact = userContact;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public List<Membership> getMemberships() {
		return memberships;
	}

	public void setMemberships(List<Membership> memberships) {
		this.memberships = memberships;
	}

	@Override
	public String toString() {
		return "User Id"+id+" [userName=" + userName + ", userContact=" + userContact + ", userAddress=" + userAddress
				+ ", userEmail=" + userEmail + ", userPassword=" + userPassword + "]";
	}
	
}
