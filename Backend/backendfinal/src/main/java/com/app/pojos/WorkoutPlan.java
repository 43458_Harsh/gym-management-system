package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name="workoutplans")
public class WorkoutPlan {
	@Id //mandatory
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate workoutDate;
	@NotBlank(message = "Time is required")
	@Length(min = 1, max = 3, message = "Invalid length of Time")
	@Column(length = 3)
	private int workoutTime;
	@NotBlank(message = "Name is required")
	@Length(min = 1, max = 10, message = "Invalid length of Name")
	@Column(length = 3)
	private String workoutName;
	private int memberId;
	public WorkoutPlan() {
		
	}
	public WorkoutPlan(Integer id, LocalDate workoutDate, int workoutTime,String workoutName) {
		super();
		this.id = id;
		this.workoutDate = workoutDate;
		this.workoutTime = workoutTime;
		this.workoutName = workoutName;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getWorkoutDate() {
		return workoutDate;
	}

	public void setWorkoutDate(LocalDate workoutDate) {
		this.workoutDate = workoutDate;
	}

	public int getWorkoutTime() {
		return workoutTime;
	}

	public void setWorkoutTime(int workoutTime) {
		this.workoutTime = workoutTime;
	}

	public String getWorkoutName() {
		return workoutName;
	}

	public void setWorkoutName(String workoutName) {
		this.workoutName = workoutName;
	}

	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	@Override
	public String toString() {
		return "WorkoutPlan [id=" + id + ", workoutDate=" + workoutDate + ", workoutName=" + workoutName + ", workoutTime=" + workoutTime + "]";
	}
	
}
