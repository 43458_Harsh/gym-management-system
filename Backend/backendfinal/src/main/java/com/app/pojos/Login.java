package com.app.pojos;
import javax.persistence.*;

@Entity
@Table(name="logins")
public class Login {
	@Id //mandatory
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(length = 20,unique = true)
	private String umtEmail;
	@Column(length = 20,nullable = false)
	private String umtPassword;
	@Enumerated(EnumType.STRING)
	private Role umtRole;
	private int mtId;
	
	
	
	public Login() {
		System.out.println("in ctor"+getClass().getName());
	}

	public Login(Integer id, String umtEmail, String umtPassword, Role umtRole,int mtId) {
		super();
		this.id = id;
		this.umtEmail = umtEmail;
		this.umtPassword = umtPassword;
		this.umtRole = umtRole;
		this.mtId = mtId;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUmtEmail() {
		return umtEmail;
	}

	public void setUmtEmail(String umtEmail) {
		this.umtEmail = umtEmail;
	}

	public String getUmtPassword() {
		return umtPassword;
	}

	public void setUmtPassword(String umtPassword) {
		this.umtPassword = umtPassword;
	}

	public Role getUmtRole() {
		return umtRole;
	}

	public void setUmtRole(Role umtRole) {
		this.umtRole = umtRole;
	}
	
	public int getMtId() {
		return mtId;
	}

	public void setMtId(int mtId) {
		this.mtId = mtId;
	}

	@Override
	public String toString() {
		return "Login [id=" + id + ", umtEmail=" + umtEmail + ", umtPassword=" + umtPassword + ", umtRole=" + umtRole
				+ ", mtId=" + mtId + "]";
	}

	
	
}
